using UnityEngine;

public class SateliteScript : MonoBehaviour
{
    public Vector3 startPosition;
    public float movementSpeed = 1f;
    public float minScale = .2f;
    public float maxScale = .35f;
    public float speedFactor = 1.5f;
    public float yPositionFactor = 2f;


    #region lifecycle
    void Start()
    {
        startPosition = transform.position;
    }

    void Update()
    {
        transform.position = startPosition + new Vector3(0, Mathf.Sin(Time.time * speedFactor) * yPositionFactor, 0);
    }
    #endregion
}
