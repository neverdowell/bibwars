using System.IO;
using UnityEngine;
using UnityEngine.UI;

public class FileManager : MonoBehaviour
{
    public Image image;
    public Text path;

    public string dataPath;


    private void Start()
    {
        string pathName = Application.persistentDataPath + "/Spaceships/Icon.png";
        image.sprite = LoadSprite(pathName);
        LoadSprite(GetPathAndFileNames(Application.persistentDataPath + "/Spaceships/", "", SearchOption.AllDirectories)[0]);      
    }

    public static Texture2D LoadTexture2D(string path)
    {
        if (File.Exists(path))
        {
            byte[] bytes = File.ReadAllBytes(path);
            Texture2D texture2D = new Texture2D(2, 2);
            texture2D.LoadImage(bytes);

            return texture2D;
        }
        else
        {
            return null;
        }
    }

    public static Sprite LoadSprite(string path)
    {
        Texture2D texture2D = LoadTexture2D(path);
        Sprite sprite = null;
        if (texture2D != null) { 
            sprite = Sprite.Create(texture2D,
                                          new Rect(0.0f, 0.0f, texture2D.width,
                                          texture2D.height),
                                          new Vector2(0.5f, 0.5f), 100.0f);
        }
        return sprite;
    }

    /// <summary>
    /// Returns application path depending on filesystem.
    /// * Unity Editor: <path to project folder>/Assets
    /// * Mac player: <path to player app bundle>/Contents
    /// * iOS player: <path to player app bundle>/<AppName.app>/Data(this folder is read only, use Application.persistentDataPath to save data).
    /// * Win/Linux player: <path to executablename_Data folder> (note that most Linux installations will be case-sensitive!)
    /// * WebGL: The absolute url to the player data file folder(without the actual data file name)
    /// * Android: Normally it points directly to the APK.If you are running a split binary build, it points to the OBB instead.
    /// * Windows Store Apps: The absolute path to the player data folder(this folder is read only, use Application.persistentDataPath to save data)
    /// see: https://docs.unity3d.com/ScriptReference/Application-dataPath.html
    /// </summary>
    /// <returns>Path to accessible data at runtime</returns>
    public static string GetInternalDataPath()
    {
        return Application.dataPath;
    }

    public static string GetExternalDataPath()
    {
        return Application.persistentDataPath;
    }

    /// <summary>
    /// Searches only in top directory. 
    /// </summary>
    /// <param name="path"></param>
    /// <returns></returns>
    public static string[] GetPathAndFileNames(string path)
    {
        return GetPathAndFileNames(path, null, SearchOption.AllDirectories);
    }

    /// <summary>
    /// Returns for a path all files with complete path
    /// </summary>
    /// <param name="path"></param>
    /// <param name="searchPattern">Use Wildcard characters: '?' for one character, '*' for  0 to n characters</param>
    /// <param name="searchOption">SearchOption.TopDirectoryOnly to ignore sub folders, else SearchOption.AllDirectories</param>
    /// <returns></returns>
    public static string[] GetPathAndFileNames(string path, string searchPattern, SearchOption searchOption)
    {
        if (searchPattern == null || searchPattern.Length == 0)
        {
            return Directory.GetFiles(path, "*", SearchOption.TopDirectoryOnly);
        } else { 
            return Directory.GetFiles(path, searchPattern, searchOption);
        }
    }
}
