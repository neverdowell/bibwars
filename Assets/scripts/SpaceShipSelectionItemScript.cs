﻿using UnityEngine;
using UnityEngine.UI;

public class SpaceShipSelectionItemScript : MonoBehaviour{

    [Header("Settings")]
    public int id;
    public SpaceShipSelectionScript spaceShipSelectionScript;

    [Header("UI")]
    public Image backgroundImage;
    public Image spaceshipImage;
    public Text nameText;




    #region helper
    public void SetSelected() {
        spaceShipSelectionScript.setSelected(id);
    }
    #endregion
}
