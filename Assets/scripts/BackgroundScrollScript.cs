using UnityEngine;

public class BackgroundScrollScript : MonoBehaviour
{
    public GameObject[] backgrounds;
    private float minYPosition;
    private float yPositionOffset;


    private float backgroundScrollSpeed;

    #region lifecycle
    void Awake()
    {
        backgroundScrollSpeed = 0.5f;//GameManager.GetInstance().levelBackgroundScrollSpeed;
        minYPosition = backgrounds[0].transform.position.y - backgrounds[1].transform.position.y;
        yPositionOffset = minYPosition * backgrounds.Length * -1;
    }

    void Update()
    {
        foreach(GameObject background in backgrounds)
        {
            background.transform.Translate(Vector3.down * backgroundScrollSpeed * Time.deltaTime);
            if (background.transform.position.y < minYPosition)
            {
                background.transform.Translate(Vector3.up * yPositionOffset);
            }
        }
    }
    #endregion
}
