using UnityEngine;

public class ShotScript : MonoBehaviour
{
    public float speed;


    private Rigidbody2D _rigidbody2D;


    private void Start()
    {
        _rigidbody2D = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        // Move shot
        _rigidbody2D.velocity = Vector3.up * speed;
    }

    private void OnTriggerEnter2D(Collider2D collider2D)
    {
        switch (collider2D.gameObject.tag)
        {
            case "enemy":
                Destroy(collider2D.gameObject);
                Destroy(gameObject);
                break;
            case "ziel":
                Destroy(gameObject);
                break;
        }
    }
}
