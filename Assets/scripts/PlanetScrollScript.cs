using UnityEngine;

public class PlanetScrollScript : MonoBehaviour
{
    public GameObject[] backgrounds;
    public float backgroundScrollSpeed = 0;
    public float maxYPosition;
    public float yPositionOffset;

    #region lifecycle
    void Awake()
    {
        //maxYPosition = (backgroundPositionY1 - backgroundPositionY2)*2;
        //yPositionOffset = maxYPosition  * backgrounds.Length;
    }

    void Update()
    {
        foreach(GameObject background in backgrounds)
        {
            background.transform.Translate(Vector3.down * backgroundScrollSpeed * Time.deltaTime);
            if (background.transform.position.y <= maxYPosition)
            {
                background.transform.position += Vector3.up * yPositionOffset;
            }
        }
    }
    #endregion
}
