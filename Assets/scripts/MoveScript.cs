﻿using UnityEngine;
using UnityEngine.Audio;

public class MoveScript : MonoBehaviour {

	public Vector3 startPosition;
	public Vector3 endPosition;
	public float speed;
	public float repeatTimer;
	public AudioSource audioSource;

	public RectTransform rectTransform;




	/* lifecycle methods */
	void Start () {
		// store start position and compute endposition
		rectTransform = GetComponent<RectTransform> ();
		startPosition = rectTransform.localPosition;
		endPosition = startPosition + endPosition;

		// if repeatTime is set, invoke repeating calls to setStartPosition
		if (repeatTimer > 0) {
			InvokeRepeating ("resetStartPosition", repeatTimer, repeatTimer);
		}
	}
		
	void Update () {
		rectTransform.localPosition = Vector3.MoveTowards (rectTransform.localPosition, endPosition, speed * Time.deltaTime);
	}




	/* helpers */
	// resets gameobject to start position
	void resetStartPosition()
	{
		rectTransform.localPosition = startPosition;
		if (audioSource) {
			audioSource.Play ();
		}
	}
}