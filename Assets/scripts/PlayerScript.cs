﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour {

    [Header("Player")]
    public SpriteRenderer playerSpriteRenderer;
    public bool isAlive = true;
	public bool canCollectCoins = true;
	public bool IsVerticalMovementAllowed = false;
	public float speed = 3;
	public int points = 0;
	public Text text;
	public float minXPosition;
	public float maxXPosition;
	public float minYPosition;
	public float maxYPosition;

    [Header("Points")]
	public GameObject goal;
	public GameObject score;
	public Text scoreText;

    [Header("Audio")]
	public AudioClip explosionSound;
	public AudioClip pickupSound;
	public AudioClip backgroundMusic;
	public AudioClip cheeringSound;

    [Header("Explosion")]
	public GameObject explosion;
	Animator animator;

	[Header("Shooting")]
	public bool isShootingAllowed = false;
	public KeyCode shootKeyCode = KeyCode.Space;
	public GameObject shotSpawner;
	public GameObject shotPrefab;
	public float shootingCoolDown = 0.3f;
	public float shotLivingTime = 0.5f;
	public float shotSpeed = 5;
	private float currentShootingCooldown = 0f;


	


	/* lifecycle methods */
	void Start (){
		animator = explosion.GetComponent<Animator>();
        playerSpriteRenderer.sprite = GameManager.GetInstance().GetSelectedSpaceShipSprite();
	}

	void Update () {
		Move();
		if (isShootingAllowed && isAlive) { 
			Shoot();
		}
	}
		
	void OnTriggerEnter2D(Collider2D collider){
		if (isAlive && canCollectCoins) {
			// If player collides with money, destroy money and add one point
			if (collider.tag.Equals ("coin")) {
				points++;
				text.text = points + " Points";
				AudioSource.PlayClipAtPoint (pickupSound, Vector3.zero);
				Destroy (collider.gameObject);
			}  // otherwise, if player hits an enemy, destroy player und invoke chanteToMenu
		else if (collider.tag.Equals ("enemy")) {
				isAlive = false;
				this.gameObject.GetComponent<Renderer> ().enabled = false;
				Destroy (collider.gameObject);
				AudioSource.PlayClipAtPoint (explosionSound, Vector3.zero);
				animator.SetTrigger ("startExplosion");
				speed = 0;
				showScoreHUD ();
			} else if (collider.tag.Equals ("ziel")) {
				showScoreHUD ();
				AudioSource.PlayClipAtPoint (cheeringSound, Vector3.zero);
				isShootingAllowed = false;
			}
		}
	}



    /* helpers */
    public void Move()
    {
		Vector3 movementDelta = Vector3.zero;
		// add horizontal movement by axis
		movementDelta += Vector3.right * Input.GetAxis("Horizontal");
		// add vertical movement by axis
		if (IsVerticalMovementAllowed)
		{
			movementDelta += Vector3.up * Input.GetAxis("Vertical");
		}
		// actualize position
		if (movementDelta.magnitude > 1f)
		{
			movementDelta.Normalize();

		}
		transform.position += movementDelta * speed * Time.deltaTime;

		// Check if player is outside left/right bounds
		if (transform.position.x < minXPosition)
		{  // check for left bound
			transform.position = new Vector3(minXPosition, transform.position.y, 0);
		}
		else if (transform.position.x > maxXPosition)
		{  // check for right bound
			transform.position = new Vector3(maxXPosition, transform.position.y, 0);
		}

		if (IsVerticalMovementAllowed)
		{
			// Check if player is outside upper/lower bounds
			if (transform.position.y < minYPosition) // check for lower bound
			{
				transform.position = new Vector3(transform.position.x, minYPosition, 0);
			}
			else if (transform.position.y > maxYPosition) // check for upper bound
			{
				transform.position = new Vector3(transform.position.x, maxYPosition, 0);
			}
		}

		// player is rotated denpending on horizontal axis speed (max 30 degrees)
		transform.rotation = Quaternion.Euler(0, 0, Input.GetAxis("Horizontal") * -30);
	}



    /* ui helpers */
    public void ChangeToMenu(){
		checkAndSetHighscore ();
		SceneManager.LoadScene ("MenuScene");
	}

	// get current highscore and check weather current score is higher and set it 
	void checkAndSetHighscore(){
		int currentHighscore = PlayerPrefs.GetInt ("highscore");
		if (currentHighscore < points) {
			PlayerPrefs.SetInt ("highscore", points);
		}
	}

	// show scoreHUD and set scoreText
	void showScoreHUD(){
		canCollectCoins = false;
		score.SetActive (true);
		scoreText.GetComponent<Text> ().text = points.ToString ();
		Invoke ("ChangeToMenu", 4f); // 1.3
	}

	public void Shoot()
	{
		currentShootingCooldown -= Time.deltaTime;
		if (Input.GetKey(shootKeyCode) && currentShootingCooldown < 0)
		{
			// reset cooldown
			currentShootingCooldown = shootingCoolDown;
			// Instantiate shot
			GameObject newShot = Instantiate(shotPrefab);
			newShot.transform.position = shotSpawner.transform.position;
			// set speed
			newShot.GetComponent<ShotScript>().speed = shotSpeed;

			// Destroy shot, when living time is over
			Destroy(newShot, shotLivingTime);
		}
	}
}
