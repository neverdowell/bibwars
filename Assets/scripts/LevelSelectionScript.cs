﻿using UnityEngine;
using UnityEngine.UI;

public class LevelSelectionScript : MonoBehaviour{

    public GameObject content;
    public ScrollRect scrollRect;
    public Scrollbar scrollbar;
    public GameObject levelSelectionItemPrefab;
    public Color selectedColor;
    public Color unselectedColor;
    
    private GameManager gameManager;
    private GameObject[] levelSelectionItems;
    private string levelNumberFormat = "00";



    #region lifecycle
    void Start(){
        gameManager = GameManager.GetInstance();
        Debug.Log("Instantiate LevelSelectionItems");
        levelSelectionItems = new GameObject[gameManager.levelPrefabs.Length];

        //for (int i = 0; i < gameManager.levelSprites.Length; i++) {
        //    Sprite sprite = gameManager.levelSprites[i];
        //    Debug.Log("Create selection item: " + sprite.name);
        //    levelSelectionItems[i] = createLevelSelectionItem(i, sprite.name, sprite);
        //}
        for (int i = 0; i < gameManager.levelPrefabs.Length; i++)
        {
//            Sprite sprite = gameManager.levelSprites[i];
            GameObject levelPrefab = gameManager.levelPrefabs[i];
            Debug.Log("Create selection item: " + levelPrefab.name);
            levelSelectionItems[i] = createLevelSelectionItem(i, levelPrefab.name, null);
        }
        setSelected(gameManager.selectedLevelId);
        scrollbar.value = 1;
        scrollRect.verticalNormalizedPosition = 1; // set scroll position to top
    }
    #endregion




    #region helper
    public GameObject createLevelSelectionItem(int id, string levelName, Sprite levelSprite) {
        GameObject levelSelectionItem = Instantiate(levelSelectionItemPrefab, content.transform);
        levelSelectionItem.name = levelName;
        LevelSelectionItemScript levelSelectionItemScript = levelSelectionItem.GetComponent<LevelSelectionItemScript>();
        levelSelectionItemScript.levelSelectionScript = this;
        levelSelectionItemScript.id = id;
        levelSelectionItemScript.levelImage.sprite = levelSprite;
        levelSelectionItemScript.levelNumberText.text = id.ToString(levelNumberFormat);
        levelSelectionItemScript.nameText.text = levelName;

        return levelSelectionItem;
    }

    public void setSelected(int selectedIndex) {
        gameManager.selectedLevelId = selectedIndex;
        for (int i = 0; i < levelSelectionItems.Length; i++) {
            Image image = levelSelectionItems[i].GetComponent<LevelSelectionItemScript>().backgroundImage;
            if (i == selectedIndex) {
                image.color = selectedColor;
            } else {
                image.color = unselectedColor;
            }
        }
    }
    #endregion
}
