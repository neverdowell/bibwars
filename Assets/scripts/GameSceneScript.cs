using UnityEngine;

public class GameSceneScript : MonoBehaviour {

    private void Awake() {
        GameObject levelGameObject = Instantiate<GameObject>(GameManager.GetInstance().GetSelectedLevelPrefab(), transform);
        levelGameObject.AddComponent<LevelScrollScript>();
    }
}
