﻿using System.IO;
using UnityEngine;

public class GameManager : MonoBehaviour {

    private const string FOLDER_RESOURCES_LOGO = "Logo"; // 400x226
    private const string FOLDER_RESOURCES_SPACESHIP_SPRITES = "Spaceships"; // 160 x 200
    private const string FOLDER_RESOURCES_LEVEL_SPRITES = "LevelSprites";
    private const string FOLDER_RESOURCES_LEVEL_PREFABS = "LevelPrefabs";

    [Header("Level Settings")]
    public int selectedLevelId = 0;
    public float levelScrollSpeed = 2;
    public float levelBackgroundScrollSpeed = 0.5f;


    // logo
    [Header("UI")]
    public Sprite logo;

    // spaceship
    [Header("Spaceships")]
    public Sprite[] spaceShipSprites;
    public int selectedSpaceShipId = 0;

    // level
    [Header("Levels")]
    public Sprite[] levelSprites;
    public GameObject[] levelPrefabs;
    




    #region singleton
    private static GameManager instance;
    private GameManager() { }

    public static GameManager GetInstance() {
        return instance;
    }

    #endregion




    #region lifecycle
    private void Awake() {
        if (instance == null) {
            instance = this;
            instance.loadGameLogo();
            instance.loadSpaceShipSprites();
            instance.loadLevelSprites();
            instance.loadLevelPrefabs();
            DontDestroyOnLoad(this.gameObject);
        } else {
            if (instance != this) {
                Destroy(this.gameObject);
            }
        }
    }
    #endregion




    #region helpers
    public void Quit()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }


    private void loadGameLogo() {
        Sprite[] loadedSprites = Resources.LoadAll<Sprite>(FOLDER_RESOURCES_LOGO);
        if (loadedSprites.Length > 0)
        {
            logo = loadedSprites[0];
        }
    }


    // spaceship helpers
    private void loadSpaceShipSprites() {
        // load internal spaceships
        Sprite[] internalSprites = Resources.LoadAll<Sprite>(FOLDER_RESOURCES_SPACESHIP_SPRITES);

        // load external spaceships in applicationData/bib International College/bibWars/Spaceships
        string path = Application.persistentDataPath + Path.DirectorySeparatorChar + FOLDER_RESOURCES_SPACESHIP_SPRITES + Path.DirectorySeparatorChar;
        Debug.Log("External Path: " + path);
        string[] externalSpritePaths = new string[0];
        if (Directory.Exists(path)) {
            Debug.Log("External path exists");
            externalSpritePaths = FileManager.GetPathAndFileNames(path, "*.png", SearchOption.AllDirectories);
        }
        // prepare spaceShipSpritesArray
        spaceShipSprites = new Sprite[internalSprites.Length + externalSpritePaths.Length]; // -1?
        Debug.Log("internalSprites.Length: " + internalSprites.Length);
        Debug.Log("externalSpritePaths.Length: " + externalSpritePaths.Length);
        Debug.Log("spaceShipSprites null? " + spaceShipSprites == null);

        // copy internal spaceShipSprites
        int index = 0;
        if (internalSprites.Length > 0)
        { 
            for (int i = 0; i < internalSprites.Length; i++)
            {
                index = i;
                spaceShipSprites[index] = internalSprites[index];
            }
        }
        // add external spaceShipSprites
        if (externalSpritePaths.Length > 0)
        {
            for (int i = 0; i < externalSpritePaths.Length; i++)
            {
                spaceShipSprites[index] = FileManager.LoadSprite(externalSpritePaths[i]);
                int startIndex = externalSpritePaths[i].LastIndexOf(Path.DirectorySeparatorChar) + 1;
                spaceShipSprites[index].name = externalSpritePaths[i].Substring(startIndex);
            }
        }
    }

    public Sprite GetSelectedSpaceShipSprite() {
        return spaceShipSprites[selectedSpaceShipId];
    }


    // level helpers
    private void loadLevelSprites() {
        levelSprites = Resources.LoadAll<Sprite>(FOLDER_RESOURCES_LEVEL_SPRITES);
    }

    private void loadLevelPrefabs() {
        levelPrefabs = Resources.LoadAll<GameObject>(FOLDER_RESOURCES_LEVEL_PREFABS);
    }

    public Sprite GetSelectedLevelSprite() {
        return levelSprites[selectedLevelId];
    }

    public GameObject GetSelectedLevelPrefab()
    {
        return levelPrefabs[selectedLevelId];
    }
    #endregion
}